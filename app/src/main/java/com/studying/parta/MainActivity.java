package com.studying.parta;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textTv;
    private TextView numberTv;

    private Button butPrint;
    private Button butClear;
    private Button butPlus;
    private Button butMinus;

    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textTv = findViewById(R.id.text);
        numberTv = findViewById(R.id.numbers);

        butPrint = findViewById(R.id.print);
        butClear = findViewById(R.id.clear);
        butPlus = findViewById(R.id.plus);
        butMinus = findViewById(R.id.minus);

        butPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textTv.setText("Hallo world!!!");
            }
        });

        butClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textTv.setText("");
            }
        });

        butPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberTv.setText(Integer.toString(++counter));
            }
        });

        butMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberTv.setText(Integer.toString(--counter));
            }
        });
    }
}
